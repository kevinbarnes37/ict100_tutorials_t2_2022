let playerWallet = 100;

let playerScore = 0;
let playerNumAces = 0;
let dealerScore = 0;
let dealerNumAces = 0;

function collectBet(){
    let isBetValid = false;
    let collectedBet;
    do {
        // ask player to place the bet
        // we retrieved the bet
        collectedBet = 10;
        // is the bet valid?
        if (collectedBet > 0 && collectedBet <= playerWallet){
            isBetValid = true;
        } else {
            console.log("Sorry, your bet is not valid!");
        }
    } while (!isBetValid);
    playerWallet -= collectedBet;

    return collectedBet;
}

function dealCard(){
    const min = 2;
    const max = 11;
    let card = min + Math.floor(Math.random() * (max - min + 1));
    return card;
}

function firstDeal(){
    for (let i = 0; i < 2; i++){
        let card = dealCard();
        if (card == 11){
            playerNumAces++;
        }
        console.log(`You got a card with value ${card}`);
        playerScore += card;
    }
    let card;
    for (let i = 0; i < 2; i++){
        card = dealCard();
        if (card == 11){
            dealerNumAces++;
        }
        if (i == 0){
            console.log(`The dealer got a card with value ${card}`);
        }
        dealerScore += card;
    }

    return card;
}

function handValue(totalScore, numAces){
    let finalScore = totalScore;

    while(finalScore > 21 && numAces > 0){
        finalScore -= 10;
        numAces--;
    }

    return finalScore;
}

function hitOrStay(){
    const threshold = 12 + Math.floor(Math.random() * 6);
    const currentValue = handValue(playerScore, playerNumAces);

    if (currentValue > threshold){
        // stay
        return false;
    } else {
        // hit
        return true;
    }
}

function doesDealerHit(){
    const threshold = 17;
    const currentValue = handValue(dealerScore, dealerNumAces);

    if (currentValue >= threshold){
        // stay
        return false;
    } else {
        // hit
        return true;
    }
}

function newGame(){
    // player places a bet
    // we need to check that the bet is valid (> 0 and < total wallet)
    let bet;
    bet = collectBet();
    console.log("The collected bet is " + bet);


    // deal the first 2 cards
    // for both the player and the dealer
    let hiddenCard = firstDeal();

    let currentPlayerScore = handValue(playerScore, playerNumAces);

    console.log(`Your score is ${currentPlayerScore}`);

    // check the total, if the player has 21 is blackjack and automatically WINS
    if (currentPlayerScore == 21){
        console.log("You got blackjack!");
        playerWallet += bet * 2.5;

        return true;
    }
    // otherwise

    // LOOP
    // player decides if hitting another card or sit
    while (hitOrStay()){
        let newCard = dealCard();
        console.log("You got a new card with value " + newCard);
        if (newCard == 11){
            playerNumAces++;
        }
        playerScore += newCard;

        let currentScore = handValue(playerScore, playerNumAces);
        console.log("Your current score is " + currentScore);
        // we check the cards, if 21 automatic WINS, if more than 21 is busted (LOST)
        // LOOP ENDS if we get 21 or more than 21 OR player sits

        if (currentScore >= 21){
            if (currentScore == 21){
                // blackjack
                console.log("You win!");
                playerWallet += bet * 2;

                return true;
            } else {
                // busted
                console.log("You lose!");
                return false;
            }
        }

    }
    
    console.log("Game still on")

    // we show the dealer cards
    console.log("The dealer hidden card is " + hiddenCard);
    console.log(`The dealer has a score of ${handValue(dealerScore, dealerNumAces)}`);
    
    // LOOP
    // we check if dealer has 17 or more, if not hit a new card
    while(doesDealerHit()){
        let newCard = dealCard();
        console.log("The dealer got a new card with value " + newCard);
        if (newCard == 11){
            dealerNumAces++;
        }
        dealerScore += newCard;

        let currentScore = handValue(dealerScore, dealerNumAces);
        console.log("The dealer current score is " + currentScore);

        if (currentScore > 21){
            // busted
            console.log("You win!");

            playerWallet += bet * 2;

            return true;
        }
    }

    // dealer is not busted: either player or dealer win (based on their score)
    // OR dealer got busted and player WIN

    let playerFinalScore = handValue(playerScore, playerNumAces);
    console.log("Your final score is: " + playerFinalScore);
    let dealerFinalScore = handValue(dealerScore, dealerNumAces);
    console.log("The dealer final score is: "+ dealerFinalScore);

    if (playerFinalScore > dealerFinalScore){
        console.log("You win!");

        playerWallet += bet * 2;

        return true;
    } else {
        console.log("You lose!");
        return false;
    }
}

newGame();